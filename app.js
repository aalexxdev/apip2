const express = require('express');
const app = express();
const db = require('./db');
const phoneController = require('./phoneController');
app.use('/phones', phoneController);
module.exports = app;