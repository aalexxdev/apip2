const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());
const phone = require('./phone');
module.exports = router;
router.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// add phone
router.post('/', (req, res, next) => {
    console.dir(req);
    phone.create({
            name: req.query.name,
            spec: req.query.spec,
            img: req.query.img,
            price: req.query.price,
            color: req.query.color
        },
        (err, phone) => {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(phone);
        });
});
// all phones
router.get('/', (req, res, next) => {
    phone.find({}, (err, phones) => {
        if (err) return res.status(500).send("There was a problem finding the phones.");
        res.status(200).send(phones);
    });
});
// get phone by ID
router.get('/:id', (req, res) => {
    phone.findById(req.params.id, (err, phone) => {
        if (err) return res.status(500).send("There was a problem finding the phone.");
        if (!phone) return res.status(404).send("No phone found.");
        res.status(200).send(phone);
    });
});

// delete phone
router.delete('/:id', (req, res) => {
    phone.findByIdAndRemove(req.query.id, function (err, phone) {
        if (err) return res.status(500).send("There was a problem deleting the phone.");
        res.status(200).send("phone " + phone.name + " was deleted.");
    });
});

//update phone
router.put('/:id', (req, res)  => {

    phone.findByIdAndUpdate(req.query.id, req.body, {new: true}, (err, phone) => {
        if (err) return res.status(500).send("There was a problem updating the phone.");
        res.status(200).send(phone);
    });
});
module.exports = router;